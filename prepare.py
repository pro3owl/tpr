import os

def get_tagger_for_lang(lang):
	if lang == 'fr':
		return 'french-utf8'
	elif lang == 'it':
		return 'italian-utf8'
	elif lang == 'bg':
		return 'bulgarian'
	elif lang == 'es':
		return 'spanish-utf8'
	else:
		raise Exception('Unknown language: ' + lang)
	
def POSfile(fin_name, fout_name, lang):
	print('taggering file: "%s"' % (fin_name))
	os.system("cat %s | treetagger/cmd/tree-tagger-%s > %s " % (fin_name, get_tagger_for_lang(lang), fout_name))
	