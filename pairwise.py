#!/bin/python3

from optparse import OptionParser
from utils import connect_local


parser = OptionParser()

parser.add_option("-l", "--lang", dest="lang", help="language to analyze")
parser.add_option("-p", "--postfix", dest="post", default="_new", help="postfix for records")

(options, args) = parser.parse_args()

if not options.lang :
    parser.error('Language not given')
elif not options.lang in ['es', 'it'] :
	raise Exception("What am i supposed to with '%s'?" % options.lang)

cur = connect_local()

name = options.lang + options.post

for field in ["POS", "Genre", "Number"]:

	cur.execute("drop table if exists pairwised_%s_%s" % (name, field))

	cur.execute("""
		create table pairwised_{0}_{1} (
				Pos varchar(3),
				Genre varchar(3),
				Number varchar(3),
				Intersections float,
				Usages float
		) ENGINE=Aria DEFAULT CHARSET=utf8;
	""".format(name, field))

	cur.execute("""
		insert into pairwised_{0}_{1} (
			{1},
			Intersections,
			Usages
		) select distinct
			concat(l.{1}, ",", r.{1}) as Pair,
			1 as Intersections,
			l.Count + r.Count as Usages 
		from
				{0} l
			inner join
				{0} r
			on
					(
							l.Norm = r.Norm
						and
							l.Word != r.Word
						and
							(
									l.POS != r.POS
								or
									l.Number != r.Number
								or
									l.Genre != r.Genre
							)
						
					)
				or
					(
							l.Word = r.Word
						and
							(
									l.Norm != r.Norm
								or
									l.POS != r.POS
								or
									l.Number != r.Number
								or
									l.Genre != r.Genre
							)
					)
	""".format(name, field))