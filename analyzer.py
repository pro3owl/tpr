#!/bin/python3

import sys
import os
from optparse import OptionParser

from prepare import POSfile
from statistics import make_statistics

parser = OptionParser()
parser.add_option("-f", "--full", dest="filename",
				  help="run POS tagger on file, count pre-statistics and load data to DB")
parser.add_option("-s", "--simple-statistics",
				  dest="simple_stat", 
				  action="store_true",
				  help='Count general statistics on existing tag-file.')
parser.add_option("-S", "--full-statistics",
				  dest="full_stat", 
				  action="store_true",
				  help='Count full statistics using the database.')
parser.add_option("-u", "--upload",
				  dest="upload",
				  action="store_true",
				  help='Upload data to database.')
parser.add_option("-y", "--yes",
				  dest="rewrite",
				  action="store_true",
				  help='Rewrite files.')

(options, args) = parser.parse_args()

if options.filename :
	filename = options.filename
	lang = filename[-2:]
	file_lm = filename + '.lm'
	if (os.path.exists(file_lm) and not options.rewrite):
		sys.stdout.write('File ' + file_lm + ' already exists. Overwrite?(y/n)[n]: ')
		answer = input()
		if ( len(answer) > 0 and answer[0] == 'y' ):
			POSfile(filename, file_lm, lang)
	else:
		POSfile(filename, file_lm, lang)
		
	make_statistics(file_lm, lang)

elif options.simple_stat :
	file_lm = args[0]
	if file_lm[-3:] != '.lm':
		print('[error] Unsupported file format (needs *.lm)')
		sys.exit()
	lang = file_lm[-5:-3]
	make_statistics(file_lm, lang, options.upload)

elif options.upload:
	file_lm = args[0]
	if file_lm[-3:] != '.lm':
		print('[error] Unsupported file format (needs *.lm)')
		sys.exit()
	lang = file_lm[-5:-3]
	make_statistics(file_lm, lang, options.upload)

else:
	parser.print_help()
