#!/usr/bin/perl

use strict;
use warnings;
use 5.016;
use utf8;
binmode STDOUT, "utf8";
use threads;


use LWP::UserAgent;
use HTML::TreeBuilder;
use Time::Local qw(timegm);
use POSIX qw(strftime);
use Getopt::Long;
use Thread::Queue;

use Getopt::Long;
my $articles = -1;
my $threads_count = 4;

GetOptions ("count=i" => \$articles, "threads=i" => \$threads_count); 

my $URL_BASE = "http://www.abc.es/";

my $fname = "abc-3-es";
if ( -e $fname && -s $fname ) {
	print "File $fname already exists. Overwrite?(y/n): ";
	while ( <> ) {
		exit(0) if /^n/i;
		last if /^y/i;
	}
}

open(my $file, ">:utf8", $fname) or die "Can't open $fname: $!";

my $day = timegm(0, 0, 0, 1, 1, 2011);
my $fin = toYYMMDD(time());

my $Qwork = new Thread::Queue;
my $Qresults = new Thread::Queue;

my @pool = map{
    threads->create( \&fetch_news, $Qwork, $Qresults )
} 1 .. $threads_count;


my $agent = new LWP::UserAgent;
	$agent->timeout(10);

my $to_write = 0;
while ( toYYMMDD($day) <= $fin && $articles != 0 ) {
	my $dayf = strftime("%d-%m-%Y", gmtime($day));
	say "Day $dayf";

	my $page = 1;
	
	while ( $page ) {
		if ( my $day_news = get_url($agent, "hemeroteca/dia-$dayf/pagina-$page") ) {
		
			my @news = map { @{ $_->extract_links('a') } } grep { ($_->{class} || "") =~ /\btitulo\b/} $day_news->look_down(_tag=>'a');
			if ( my @articles = grep { $articles ? ($articles-- || 1) : 0 } @news ) {
				$Qwork->enqueue( $_->[0] ) foreach @articles;
				my @readed = $Qresults->dequeue_timed(3, $to_write + @articles);
				$to_write += @articles - @readed;
				say $file  grep { $_ } @readed;
			}
			last unless $articles;
			last unless @news;
		}
		$page++;
	}
	$day += 24 * 3600;
}

$Qwork->enqueue( (undef) x $threads_count );

$_->join for @pool;

say $file grep { $_ } $Qresults->dequeue_timed(60, $to_write) if $to_write;

close $file;

sub get_url {
	my $agent = shift;
	my $url = shift;
	my $skip_base = shift || 0;
	my $full = ($skip_base ? "" : $URL_BASE) . $url;

	my $req = HTTP::Request->new('GET'); 
	$req->url($full);

	my $resp = $agent->request($req);
	if ( $resp->is_success ) {	
		return HTML::TreeBuilder->new_from_content( scalar $resp->decoded_content );
	} else {
		warn "Cant fetch '$full': $!"
		and return;
	}
}

sub toYYMMDD {
	my $date = shift;
	return strftime("%Y%m%d", gmtime($date));
}

sub fetch_news {
	my $agent = new LWP::UserAgent;
	$agent->timeout(10);
	my $tid = threads->tid;
    my( $Qwork, $Qresults ) = @_;
	while( my $url = $Qwork->dequeue ) {
		if ( my $p = get_url($agent, $url, 1) ) {
			if ( my @res = grep  { ($_->{class} || "") =~ /\btext\b/}  $p->look_down(_tag=>'div') ) {
				$Qresults->enqueue((map {
					my $parent = $_;
					$_->delete_content() foreach $parent->find_by_tag_name("script");
					$parent->as_text() || ""
				} @res)[0])
			} else {
				$Qresults->enqueue("")
			}
		} else {
			$Qresults->enqueue("")
		}
	}
	$Qresults->enqueue( undef );
}