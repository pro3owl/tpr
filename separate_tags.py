#!/bin/python3

import sys

from statistics import init_lang
from statistics import is_word

def twice_file(filename, charset='utf-8'):
	tagset = open(filename, 'r', encoding=charset)
	not_words = open(filename + '.not_words', 'w', encoding=charset)
	are_words = open(filename + '.are_words', 'w', encoding=charset)
	for line in tagset:
		if is_word('', line.split('\t')[0], 'thisisword'):
			are_words.write(line)
		else:
			not_words.write(line)
		#sys.exit(0)
	not_words.close()
	are_words.close()
	

try:
	init_lang(sys.argv[1])
	filename = sys.argv[2]
	try:
		twice_file(filename)
	except UnicodeDecodeError:
		print('[error]: UnicodeDecodeError, trying latin-1')
		twice_file(filename, 'latin-1')
except Exception:
	print("usage: <lang(es,fr,it,...)> <tagset_filename>")
	print('[error]: %s' % (sys.exc_value()))