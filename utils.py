import pymysql
import prettytable
import os
import sys

POS_DEPARSE = {
	'A': 'ADJ',
	'R': 'ADV',
	'D': 'DET',
	'N': 'NOUN',
	'V': 'VERB',
	'P': 'PRON',
	'C': 'CONJ',
	'I': 'INTERJ',
	'S': 'PREP',
	'F': 'PUNCT',
	'Z': 'NUM',
	'W': 'DATE',
}

NUMBER_DEPARSE = {
	'S': 'SINGLE',
	'P': 'PLURAL',
	'N': 'INVAR',
}

COMMON_PARENT = {"known": "all_tokens",
		"unknown": "all_tokens",
		"words": "all_tokens",
		"not_words": "all_tokens",
		"whole_norms": "words",
		"unique_usages": "whole_norms",
		"several_norms": "whole_norms",
		"usage_with_number": "whole_norms",
		"usage_with_genre_and_number": "whole_norms",
		"usage_with_genre": "whole_norms"}

COMMON_ALIAS = { "all_tokens": "All tokens",
		"known": "Known tokens",
		"unknown": "Unknown tokens",
		"words": "Words",
		"not_words": "Not words",
		"whole_norms": "Wordforms count",
		"unique_usages": "Unique usages",
		"several_norms": "Words with several norms",
		"usage_with_number": "With number form",
		"usage_with_genre_and_number": "With genre and number form",
		"usage_with_genre": "With genre form"}

PRINT_ORDER = [ {"all_tokens": ["words", "not_words", "unknown"]},
				{"words": ["whole_norms"]},
				{"whole_norms": ["unique_usages", "several_norms",
								 "usage_with_number", "usage_with_genre_and_number",
								 "usage_with_genre"]}]


def connect_local():
	db = pymysql.connect(host="localhost", user="root", charset='utf8', db="langs")
	cursor = db.cursor()
	cursor.execute("SET SESSION group_concat_max_len = 1000000;")
	return cursor

def load_qeury_from_file(filename):
	sql_command = ''
	if filename[-4:] != '.sql':
		filename += '.sql'
	if not os.path.exists(filename):
		filename = 'sql/' + filename
	with open(filename) as f:
		for line in f:
			sql_command += line
	return sql_command

def percent(val, full):
	return "%.1f%%" % (1.0*val/full*100)

def common_stat(cur, lang, descent):
	select_values = ["Stat", "Value", "Percent"]
	x = prettytable.PrettyTable(select_values)
	x.align["Stat"] = "l"
	x.align["Value"] = "r"
	cur.execute(load_qeury_from_file("common_stat").format(",".join(select_values[:-1]), lang))

	values = {}
	for row in cur:
		values[row[0]] = int(row[1])

	new_line = lambda : x.add_row(["", "", ""])
	add_row_parent = lambda key: x.add_row([COMMON_ALIAS[key], values[key], "-"])
	add_row = lambda key: x.add_row([COMMON_ALIAS[key], values[key], percent(values[key], values[COMMON_PARENT[key]])])

	for node in PRINT_ORDER:
		for parent in node:
			add_row_parent(parent)
			for child in node[parent]:
				add_row(child)
		new_line()
	cur.execute(load_qeury_from_file("omonims_full_count").format(lang))

	stat = []
	for row in cur:
		stat.append([int(row[0]), int(row[1])])

	unique_by_word = stat[0][0]
	full_by_word = stat[0][1]
	tokens_by_word = stat[1][0]

	homo_by_field = []
	for key in [["POS"], ["Norm"], ["Number"], ["Genre"],
				# pairs
				["POS", "Norm"], ["POS", "Number"], ["POS", "Genre"],
				["Norm", "Number"], ["Number", "Genre"], ["Norm", "Genre"],
				# triangles
				["POS", "Norm", "Number"], ["POS", "Norm", "Genre"], ["POS", "Number", "Genre"],
				["Norm", "Number", "Genre"],
				# quatro
				["POS", "Norm", "Number", "Genre"]]:
		homo_by_field.append((key, full_homonymy_count_by_field(cur, lang, key)))


	add_row_parent("words")
	x.add_row(["Homonymy by Wordform", tokens_by_word, percent(tokens_by_word, values["words"])])
	sum_ = 0
	for (key, counts) in homo_by_field:
		(unique_count, full_count, tokens_count) = counts
		x.add_row(["Homonymy by " + ", ".join(key), tokens_count, percent(tokens_count, values["words"])])
		sum_ += tokens_count
	x.add_row(["SUM (must be equal to homonymy by Wordform)", sum_, ''])

	new_line()

	add_row_parent("unique_usages")
	x.add_row(["Unique homonymy by Wordform", unique_by_word, percent(unique_by_word, values["unique_usages"])])
	sum_ = 0
	for (key, counts) in homo_by_field:
		(unique_count, full_count, tokens_count) = counts
		x.add_row(["Unique homonymy by "+", ".join(key), unique_count, percent(unique_count, values["unique_usages"])])
		sum_ += unique_count
	x.add_row(["SUM (must be equal to unique homonymy by Wordform)", sum_, ''])

	new_line()

	add_row_parent("whole_norms")
	x.add_row(["Total homonymy by Wordform", full_by_word, percent(full_by_word, values["whole_norms"])])
	sum_ = 0
	for (key, counts) in homo_by_field:
		(unique_count, full_count, tokens_count) = counts
		x.add_row(["Total homonymy by "+", ".join(key), full_count, percent(full_count, values["whole_norms"])])
		sum_ += full_count
	x.add_row(["SUM (must be equal to Total homonymy by Wordform)", sum_, ''])

	print(x.get_string(reversesort=descent))

def full_homonymy_count(cur, lang):
	cur.execute(load_qeury_from_file("omonims_full_count").format(lang))
	stat = []
	for row in cur:
		stat.append([int(row[0]), int(row[1])])
	unique_homonymy = stat[0][0]
	total_homonymy = stat[0][1]
	tokens_homonymy = stat[1][0]

	return (unique_homonymy, total_homonymy, tokens_homonymy)

def full_homonymy_count_by_field(cur, lang, fields):
	parameters = [field for field in fields]
	parameters.insert(0, lang)
	for minus_field in ['POS', 'Norm', 'Number', 'Genre']:
		if not minus_field in parameters:
			parameters.append(minus_field)

	if len(fields) == 1:
		filename = "tuples_by_lex_par"
	elif len(fields) == 2:
		filename = "homonymy_by_fields_2"
	elif len(fields) == 3:
		filename = "homonymy_by_fields_3"
	elif len(fields) == 4:
		filename = "homonymy_by_fields_4"

	cur.execute(load_qeury_from_file(filename).format(*parameters))
	unique_homonymy = 0
	total_homonymy = 0
	tokens_homonymy = 0
	for row in cur:
		unique_homonymy += int(row[0])
		total_homonymy += int(row[1])
		tokens_homonymy += int(row[2])
	return (unique_homonymy, total_homonymy, tokens_homonymy)

def deparse_pos(str):
	global POS_DEPARSE
	res = []
	for pos in str.split(','):
		if pos in POS_DEPARSE:
			res.append( POS_DEPARSE[pos] )
		else :
			res.append(pos)
	return ",".join(res)

def deparse_number(str):
	global NUMBER_DEPARSE
	res = []
	for pos in str.split(','):
		if pos in NUMBER_DEPARSE:
			res.append( NUMBER_DEPARSE[pos] )
		else :
			res.append(pos)
	return ",".join(res)


def grouped_stat(cur, name, target_field):
	cur.execute(load_qeury_from_file("pos_tuples").format(name, target_field))
	return cur

def separated_grouped_stat(cur, name, target_field):
	parameters = [name, target_field]
	for minus_field in ['POS', 'Norm', 'Number', 'Genre']:
		if not minus_field in parameters:
			parameters.append(minus_field)
	cur.execute(load_qeury_from_file("tuples_by_lex_par").format(*parameters))
	return cur

def pairwised_stat(cur, name, target_field):
	cur.execute("""
		select
			{1},
			sum(Intersections) as Intersections,
			sum(Usages) as Usages
		from
			pairwised_{0}_{1}
		group by
			{1}
	""".format(name, target_field))
