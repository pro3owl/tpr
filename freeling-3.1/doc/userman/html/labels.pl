# LaTeX2HTML 2008 (1.71)
# Associate labels original text with physical files.


$key = q/file-hmm/;
$external_labels{$key} = "$URL/" . q|node49.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_brants00/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-dates/;
$external_labels{$key} = "$URL/" . q|node31.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_vossen98a/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/chap-modules/;
$external_labels{$key} = "$URL/" . q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/t-langs/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/file-alter/;
$external_labels{$key} = "$URL/" . q|node45.html|; 
$noresave{$key} = "$nosave";

$key = q/ssec-data/;
$external_labels{$key} = "$URL/" . q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-requirements/;
$external_labels{$key} = "$URL/" . q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/mod-coref/;
$external_labels{$key} = "$URL/" . q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-installation/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-maco/;
$external_labels{$key} = "$URL/" . q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/c-adding-lang/;
$external_labels{$key} = "$URL/" . q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/file-sense/;
$external_labels{$key} = "$URL/" . q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_fellbaum98/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_atserias05/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-tagset/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_padro98a/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/semdb/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_carreras03/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-prob/;
$external_labels{$key} = "$URL/" . q|node43.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-foma/;
$external_labels{$key} = "$URL/" . q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-pattern/;
$external_labels{$key} = "$URL/" . q|node78.html|; 
$noresave{$key} = "$nosave";

$key = q/file-split/;
$external_labels{$key} = "$URL/" . q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-tgz/;
$external_labels{$key} = "$URL/" . q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-pos/;
$external_labels{$key} = "$URL/" . q|node48.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-deb/;
$external_labels{$key} = "$URL/" . q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/file-dep/;
$external_labels{$key} = "$URL/" . q|node60.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-svn/;
$external_labels{$key} = "$URL/" . q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/file-wn/;
$external_labels{$key} = "$URL/" . q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/tr-tags/;
$external_labels{$key} = "$URL/" . q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/file-tok/;
$external_labels{$key} = "$URL/" . q|node21.html|; 
$noresave{$key} = "$nosave";

$key = q/file-suf/;
$external_labels{$key} = "$URL/" . q|node34.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-custom/;
$external_labels{$key} = "$URL/" . q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/mod-ukb/;
$external_labels{$key} = "$URL/" . q|node47.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_karlsson95/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-phon/;
$external_labels{$key} = "$URL/" . q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/file-mw/;
$external_labels{$key} = "$URL/" . q|node36.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-cond/;
$external_labels{$key} = "$URL/" . q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/file-relax/;
$external_labels{$key} = "$URL/" . q|node50.html|; 
$noresave{$key} = "$nosave";

$key = q/wnmap/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/file-usermap/;
$external_labels{$key} = "$URL/" . q|node29.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-rules/;
$external_labels{$key} = "$URL/" . q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_soon01/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/ss-options/;
$external_labels{$key} = "$URL/" . q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/fex-file/;
$external_labels{$key} = "$URL/" . q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-rgf/;
$external_labels{$key} = "$URL/" . q|node75.html|; 
$noresave{$key} = "$nosave";

$key = q/file-cfg/;
$external_labels{$key} = "$URL/" . q|node58.html|; 
$noresave{$key} = "$nosave";

$key = q/file-nec/;
$external_labels{$key} = "$URL/" . q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chang11/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-ner/;
$external_labels{$key} = "$URL/" . q|node38.html|; 
$noresave{$key} = "$nosave";

$key = q/lang-ident/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_agirre09/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/mod-sense/;
$external_labels{$key} = "$URL/" . q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/file-quant/;
$external_labels{$key} = "$URL/" . q|node41.html|; 
$noresave{$key} = "$nosave";

$key = q/file-dict/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/ss-config/;
$external_labels{$key} = "$URL/" . q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-execute/;
$external_labels{$key} = "$URL/" . q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-main/;
$external_labels{$key} = "$URL/" . q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/cap-analyzer/;
$external_labels{$key} = "$URL/" . q|node86.html|; 
$noresave{$key} = "$nosave";

$key = q/file-numb/;
$external_labels{$key} = "$URL/" . q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/file-punt/;
$external_labels{$key} = "$URL/" . q|node27.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2008 (1.71)
# labels from external_latex_labels array.


$key = q/file-hmm/;
$external_latex_labels{$key} = q|3.17.1|; 
$noresave{$key} = "$nosave";

$key = q/file-dates/;
$external_latex_labels{$key} = q|3.8|; 
$noresave{$key} = "$nosave";

$key = q/chap-modules/;
$external_latex_labels{$key} = q|3|; 
$noresave{$key} = "$nosave";

$key = q/t-langs/;
$external_latex_labels{$key} = q|1.3|; 
$noresave{$key} = "$nosave";

$key = q/file-alter/;
$external_latex_labels{$key} = q|3.14|; 
$noresave{$key} = "$nosave";

$key = q/ssec-data/;
$external_latex_labels{$key} = q|5.1.1|; 
$noresave{$key} = "$nosave";

$key = q/mod-coref/;
$external_latex_labels{$key} = q|3.22|; 
$noresave{$key} = "$nosave";

$key = q/sec-requirements/;
$external_latex_labels{$key} = q|2.1|; 
$noresave{$key} = "$nosave";

$key = q/sec-installation/;
$external_latex_labels{$key} = q|2.2|; 
$noresave{$key} = "$nosave";

$key = q/sec-maco/;
$external_latex_labels{$key} = q|3.4|; 
$noresave{$key} = "$nosave";

$key = q/c-adding-lang/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/file-sense/;
$external_latex_labels{$key} = q|4.2.2|; 
$noresave{$key} = "$nosave";

$key = q/file-tagset/;
$external_latex_labels{$key} = q|4.1|; 
$noresave{$key} = "$nosave";

$key = q/semdb/;
$external_latex_labels{$key} = q|4.2|; 
$noresave{$key} = "$nosave";

$key = q/file-prob/;
$external_latex_labels{$key} = q|3.13|; 
$noresave{$key} = "$nosave";

$key = q/sec-foma/;
$external_latex_labels{$key} = q|4.3|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-pattern/;
$external_latex_labels{$key} = q|4.4.3|; 
$noresave{$key} = "$nosave";

$key = q/file-split/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-tgz/;
$external_latex_labels{$key} = q|2.2.2|; 
$noresave{$key} = "$nosave";

$key = q/sec-pos/;
$external_latex_labels{$key} = q|3.17|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-deb/;
$external_latex_labels{$key} = q|2.2.1|; 
$noresave{$key} = "$nosave";

$key = q/file-dep/;
$external_latex_labels{$key} = q|3.21.1|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-svn/;
$external_latex_labels{$key} = q|2.2.3|; 
$noresave{$key} = "$nosave";

$key = q/file-wn/;
$external_latex_labels{$key} = q|4.2.3|; 
$noresave{$key} = "$nosave";

$key = q/tr-tags/;
$external_latex_labels{$key} = q|4.1.1|; 
$noresave{$key} = "$nosave";

$key = q/file-tok/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/file-suf/;
$external_latex_labels{$key} = q|3.9.2|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-custom/;
$external_latex_labels{$key} = q|4.4.5|; 
$noresave{$key} = "$nosave";

$key = q/mod-ukb/;
$external_latex_labels{$key} = q|3.16|; 
$noresave{$key} = "$nosave";

$key = q/file-phon/;
$external_latex_labels{$key} = q|3.18|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-cond/;
$external_latex_labels{$key} = q|4.4.4|; 
$noresave{$key} = "$nosave";

$key = q/file-mw/;
$external_latex_labels{$key} = q|3.10|; 
$noresave{$key} = "$nosave";

$key = q/file-relax/;
$external_latex_labels{$key} = q|3.17.2|; 
$noresave{$key} = "$nosave";

$key = q/wnmap/;
$external_latex_labels{$key} = q|4.2.1|; 
$noresave{$key} = "$nosave";

$key = q/file-usermap/;
$external_latex_labels{$key} = q|3.7|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-rules/;
$external_latex_labels{$key} = q|4.4.2|; 
$noresave{$key} = "$nosave";

$key = q/ss-options/;
$external_latex_labels{$key} = q|6.4.1|; 
$noresave{$key} = "$nosave";

$key = q/fex-file/;
$external_latex_labels{$key} = q|4.4.1|; 
$noresave{$key} = "$nosave";

$key = q/sec-rgf/;
$external_latex_labels{$key} = q|4.4|; 
$noresave{$key} = "$nosave";

$key = q/file-cfg/;
$external_latex_labels{$key} = q|3.20.1|; 
$noresave{$key} = "$nosave";

$key = q/file-nec/;
$external_latex_labels{$key} = q|3.19|; 
$noresave{$key} = "$nosave";

$key = q/file-ner/;
$external_latex_labels{$key} = q|3.11|; 
$noresave{$key} = "$nosave";

$key = q/lang-ident/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/mod-sense/;
$external_latex_labels{$key} = q|3.15|; 
$noresave{$key} = "$nosave";

$key = q/file-quant/;
$external_latex_labels{$key} = q|3.12|; 
$noresave{$key} = "$nosave";

$key = q/file-dict/;
$external_latex_labels{$key} = q|3.9|; 
$noresave{$key} = "$nosave";

$key = q/ss-config/;
$external_latex_labels{$key} = q|6.4.2|; 
$noresave{$key} = "$nosave";

$key = q/sec-execute/;
$external_latex_labels{$key} = q|2.3|; 
$noresave{$key} = "$nosave";

$key = q/sec-main/;
$external_latex_labels{$key} = q|5.2|; 
$noresave{$key} = "$nosave";

$key = q/cap-analyzer/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/file-numb/;
$external_latex_labels{$key} = q|3.5|; 
$noresave{$key} = "$nosave";

$key = q/file-punt/;
$external_latex_labels{$key} = q|3.6|; 
$noresave{$key} = "$nosave";

1;

