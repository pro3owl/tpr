# LaTeX2HTML 2008 (1.71)
# Associate images original text with physical files.


$key = q/rightarrow;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$\rightarrow$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$x$">|; 

$key = q/^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$^+$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$1$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$i$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$0$">|; 

$key = q/alpha_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$\alpha_{ij}$">|; 

$key = q/_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$_1$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$\lambda$">|; 

$key = q/-2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$-2$">|; 

$key = q/_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$_N$">|; 

$key = q/b_{iw};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$b_{iw}$">|; 

$key = q/pi_{i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$\pi_{i}$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$s$">|; 

1;

