#!/bin/python3

import prettytable
from optparse import OptionParser
from utils import *
from itertools import combinations

FIELDS = ['POS', 'Genre', 'Number']

parser = OptionParser()

parser.add_option("-l", "--lang", dest="lang", help="language to analyze")
parser.add_option("-p", "--postfix", dest="post", default="_new", help="postfix for records")
parser.add_option("-c", "--common", dest="common", action="store_true", help="print common stat")
parser.add_option("-g", "--grouped", dest="grouped", action="store_true", help="print common stat")
parser.add_option("-s", "--sort", dest="sort", default=1, type="int", help="sort column")
parser.add_option("-d", "--descent", dest="descent", action="store_true", default=False, help="descent sort")
parser.add_option("-f", "--field", dest="field", default="POS", help="target homonymy field")
parser.add_option("--separate", dest="separated_stat", action="store_true", help="count separated stats for given field")

(options, args) = parser.parse_args()

if not options.lang :
    parser.error('Language not given')
elif not options.lang in ['es', 'it'] :
	raise Exception("What am i supposed to with '%s'?" % options.lang)

if options.field not in FIELDS:
	parser.error("Bad field '%s' is given, need some of " % options.field + str(FIELDS))

if options.sort not in range(6):
	print("Invalid sort column '%s' is given, use 1" % options.sort)
	options.sort = 1

cur = connect_local()

name = options.lang + options.post

if options.common:
	common_stat(cur, name, options.descent)

target_field = options.field
other_fields = list(filter(lambda el: el != target_field, FIELDS))

if options.separated_stat:
	cur = separated_grouped_stat(cur, name, target_field)
else:
	cur = grouped_stat(cur, name, target_field)

unique_homonimy_count = 0
homonimy_count = 0
stat = {}
for row in cur:
	(sum_count, sum_sum, sum_tokens, genres, numbers, variable) = row
	sum_tokens = sum(map(lambda el: int(float(el.split(":")[-1])), set(str(sum_tokens).split(","))))
	if options.grouped :
		group = ",".join(sorted(set(variable.split(','))))
		if not group in stat : stat[ group ] = [0, 0, 0, set(), set()]
		stat[ group ][0] += int(sum_tokens)
		stat[ group ][1] += int(sum_count)
		stat[ group ][2] += int(sum_sum)
		if target_field == 'POS':
			stat[ group ][3].update(genres.split(","))
			stat[ group ][4].update(numbers.split(","))
	else :
		poss = variable.split(',')
		for pair in combinations(set(poss), 2):
			spair = ','.join(sorted(pair))
			if not spair in stat : stat[spair] = [0, 0, 0]
			stat[spair][0] += int(sum_tokens)
			stat[spair][1] += int(sum_count)
			stat[spair][2] += int(sum_sum)
		for pos in set(poss):
			if poss.count(pos) > 1 :
				spair = "%s,%s" % (pos, pos)
				if not spair in stat : stat[spair] = [0, 0, 0]
				stat[spair][0] += int(sum_tokens)
				stat[spair][1] += int(sum_count)
				stat[spair][2] += int(sum_sum)

def deparser(group):
	if target_field == 'POS' :
		return deparse_pos(group)
	elif target_field == 'Number' :
		return deparse_number(group)
	else :
		return group

(unique_homonimy_count, homonimy_count, tokens_homonymy) = full_homonymy_count(cur, name)

if options.grouped :
	header = ["Group", "Word usages", "% of words", "Unique usages", "% of unique", "All usages", "% of all"]
	if target_field == 'POS': header += ["Genre", "Number"]
	x = prettytable.PrettyTable(header)
	x.align["Word usages"] = 'r'
	x.align["% of words"] = 'r'
	x.align["Unique usages"] = 'r'
	x.align["% of all unique"] = 'r'
	x.align["All usages"] = 'r'
	x.align["% of all usages"] = 'r'
	x.hrules = prettytable.ALL
	for group, stat in stat.items() :
		if target_field == 'POS':
			x.add_row([ deparser(group),
					   stat[0],
					   percent(stat[0], tokens_homonymy),
					   stat[1],
					   percent(stat[1], unique_homonimy_count),
					   stat[2],
					   percent(stat[2], homonimy_count),
					   ",".join(sorted(stat[3], reverse=True)),
					   deparse_number(",".join(sorted(stat[4], reverse=True)))
					  ])
		else: x.add_row([ deparser(group),
						 stat[0],
						 percent(stat[0], tokens_homonymy),
						 stat[1],
						 percent(stat[1], unique_homonimy_count),
						 stat[2],
						 percent(stat[2], homonimy_count) ])

	print(x.get_string(sortby=header[options.sort], reversesort=options.descent))
else :
	kinds = map(lambda k: deparser(k).split(","), stat.keys())
	kinds = [kind for group in kinds for kind in group]
	header = sorted(set(kinds), reverse=options.descent)
	header.insert(0, "Word/Unique/All   Hom")
	stat_table = [["-" for c in header] for r in header]
	for group, stat in stat.items() :
		for pair in combinations(map(lambda name : header.index(name), deparser(group).split(",")), 2):
			str_value = "%d (%s)\n%d (%s)\n%d (%s)" % (
											stat[0] / 2,
											percent(stat[0] / 2, tokens_homonymy),
											stat[1] / 2,
											percent(stat[1] / 2, unique_homonimy_count),
											stat[2] / 2,
											percent(stat[2] / 2, homonimy_count))

			stat_table[pair[0]][pair[1]] = str_value
			stat_table[pair[1]][pair[0]] = str_value


	x = prettytable.PrettyTable(header)
	x.hrules = prettytable.ALL
	for i, row in enumerate(stat_table[1:]):
		row[0] = header[i + 1]
		x.add_row(row)

	print(x.get_string())
