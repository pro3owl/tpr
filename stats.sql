-- По частям речи

select POS, sum(Count) as Count, sum(Sum) as Sum
from (
    select Word, group_concat(distinct POS order by POS) as POS, count(*) as Count, sum(Count) as Sum
    from
        es
    where
        Word in (
            select Word
            from (
                select
                    Word,
                    count(distinct POS) as cnt1,
                    count(distinct Norm) as cnt2
                from
                    es
                group by Word
                having cnt1 > 1 or cnt2 > 1
            ) t
        )
    group by Word
    
    union all
    
    select Norm, group_concat(distinct POS order by POS) as POS, count(*) as Count, sum(Count) as Sum
    from
        es
    where
        Norm in (
            select Norm
            from (
                select
                    Norm,
                    count(distinct POS) as cnt1,
                    count(distinct Word) as cnt2
                from
                    es
                group by Norm
                having cnt1 > 1 and cnt2 > 1
            ) t
        )
        and not Word in (
            select Word
            from (
                select
                    Word,
                    count(distinct POS) as cnt1,
                    count(distinct Norm) as cnt2
                from
                    es
                group by Word
                having cnt1 > 1 or cnt2 > 1
            ) t
        )
    group by Norm

) t
group by POS
order by Count
;
