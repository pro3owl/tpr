#!/usr/bin/perl

use strict;
use warnings;
use 5.016;
use utf8;
binmode STDOUT, "utf8";
use threads;

use LWP::UserAgent;
use HTML::TreeBuilder;
use Getopt::Long;
use Thread::Queue;

my $articles = -1;
my $threads_count = 4;
my $lang = "";
GetOptions ("count=i" => \$articles, "lang=s", \$lang, "threads=i" => \$threads_count); 

my %LANGS = map {$_ => 1 } qw(fr de it es);

die "usage: \n\t-l <language> - supported langs: " . join("/", keys %LANGS) . " \n\t-c <number> - count of fetched papers\n" unless $LANGS{$lang};
my $URL_BASE = "http://$lang.euronews.com";

my $fname = "euronews-$lang";
if ( -e $fname && -s $fname ) {
	print "File $fname already exists. Overwrite?(y/n): ";
	while ( <> ) {
		exit(0) if /^n/i;
		last if /^y/i;
	}
}

open(my $file, ">utf8", $fname) or die "Can't open $fname: $!";

my $Qwork = new Thread::Queue;
my $Qresults = new Thread::Queue;

my @pool = map{
    threads->create( \&fetch_news, $Qwork, $Qresults )
} 1 .. $threads_count;


my $agent = new LWP::UserAgent;

YEAR: foreach my $year ( 2004 .. 2014 ) {
	say "Year $year";

	my $year_news = get_url($agent, "/$year")
	or next;

	my $days = $year_news->look_down(_tag=>'table', class=>'yearListing')->find_by_tag_name('tbody')->extract_links('a');
	foreach my $day ( sort map { $_->[0] } @$days ) {
		next unless $day =~ m@/\d+/\d+/\d+/@;
		say $day;

		my $day_content = get_url($agent, $day)
		or next;

		my @news = map { @{ $_->extract_links('a') } } grep { my $class = "" . ($_->attr("class") || ""); $class =~ /article-list/  } $day_content->look_down(_tag => 'ul');
		if ( my @articles = grep { $_->[0] =~ m@/\d+/\d+/\d+/@ && ($articles ? ($articles-- || 1) : 0) } @news ) {
			$Qwork->enqueue( $_->[0] ) foreach @articles;
			my $readed = 0;
			while ( $readed++ != @articles ) {
				say $file scalar($Qresults->dequeue());
			}
		}
		last YEAR unless $articles;
	}
}

$Qwork->enqueue( (undef) x $threads_count );

$_->join for @pool;

close $file;

sub get_url {
	my $agent = shift;
	my $url = shift;

	my $full = $URL_BASE . $url;

	my $req = HTTP::Request->new('GET' => $full);

	my $resp = $agent->request($req);
	if ( $resp->is_success ) {	
		return HTML::TreeBuilder->new_from_content( scalar $resp->decoded_content );
	} else {
		warn "Cant fetch '$full': $!"
		and return;
	}
}

sub fetch_news {
	my $agent = new LWP::UserAgent;
	my $tid = threads->tid;
    my( $Qwork, $Qresults ) = @_;
	while( my $url = $Qwork->dequeue ) {
		if ( my $p = get_url($agent, $url) ) {
			if ( my $res = $p->look_down(_tag=>'div',id=>'articleTranscript') ) {
				$Qresults->enqueue($res->as_text);
			} else {
				$Qresults->enqueue("")
			}
		} else {
			$Qresults->enqueue("")
		}
	}
	$Qresults->enqueue( undef );
}