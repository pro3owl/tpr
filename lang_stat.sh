#!/bin/bash
set -e
declare -A langs=( ["it"]="Итальянский" ["es"]="Испанский" )

[ ${langs[$1]} ] || (echo "Bad language '$1'" && exit 1)

mkdir -p stat
out=stat/$1.stat
out_t=stat/$1.stat_t
echo "${langs[$1]}" > $out
echo -e "\nОбщая статистика" >> $out
./pos_homonymy.py --lang $1 --common --sort 1 --field POS -g -d >> $out
perl -plnE 'print("\nПо частям речи (сортировка по частоте в тексте)") && ($single = 1) if !$single && length >= 80' $out > $out_t
cat $out_t > $out
echo -e "\nПо частям речи (сортировка по уникальным словоформам)" >> $out
./pos_homonymy.py --lang $1 --sort 3 --field POS -g -d >> $out
echo -e "\nПо частям речи (сортировка по словоупотреблениям)" >> $out
./pos_homonymy.py --lang $1 --sort 5 --field POS -g -d >> $out
echo -e "\nПо частям речи (омонимия только по частям речи)" >> $out
./pos_homonymy.py --lang $1 --sort 3 --field POS --separate -g -d >> $out
echo -e "\nМатрица по частям речи" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field POS >> $out
echo -e "\nМатрица по частям речи (омонимия только по частям речи)" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field POS --separate >> $out

echo -e "\nГруппировки по числу" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Number -g -d >> $out
echo -e "\nГруппировки по числу (омонимия только по числу)" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Number -g -d --separate >> $out
echo -e "\nМатрица по числу" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Number >> $out
echo -e "\nМатрица по числу (омонимия только по числу)" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Number --separate >> $out

echo -e "\nГруппировки по родам" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Genre -g -d >> $out
echo -e "\nГруппировки по родам (омонимия только по роду)" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Genre -g -d --separate >> $out
echo -e "\nМатрица по родам" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Genre >> $out
echo -e "\nМатрица по родам (омонимия только по роду)" >> $out
./pos_homonymy.py --lang $1 --sort 1 --field Genre --separate >> $out
perl -plnE 's/\b(g)enre\b([\w\s]*?) ([ |])/\1ender\2\3/i' $out > $out_t
cat $out_t > $out
echo $out
