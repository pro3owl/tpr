select
	sum(Count) as Count,
	sum(Sum) as Sum,
	group_concat(Tokens) as Tokens,
	group_concat(distinct Genres) as Genres,
	group_concat(distinct Numbers) as Numbers,
	{1}
from
	(
			select
				stat.Word,
				group_concat(stat.{1}) as {1},
				count(*) as Count,
				sum(stat.Count) as Sum,
				group_concat(distinct concat(tokens.Word, ":", tokens.Count)) as Tokens,
				group_concat(distinct stat.Genre) as Genres,
				group_concat(distinct stat.Number) as Numbers
			from
				{0} as stat
			inner join
				{0}_tokens as tokens using(Word)
			group by
				stat.Word
			having
					count(distinct stat.{1}) > 1
				and
					(count(distinct stat.POS) > 1 or count(distinct stat.Norm) > 1 or count(distinct stat.Number) > 1 or count(distinct stat.Genre) > 1)
		
		union all
		
			select
				stat.Norm,
				group_concat(stat.{1}) as {1},
				count(*) as Count,
				sum(stat.Count) as Sum,
				group_concat(distinct concat(tokens.Word, ":", tokens.Count)) as Tokens,
				group_concat(distinct stat.Genre) as Genres,
				group_concat(distinct stat.Number) as Numbers
			from
				{0} stat
			inner join
				{0}_tokens as tokens using(Word)
			where
				not stat.Word in (
					select
						Word
					from
						{0}
					group by
						Word
					having
							count(distinct {1}) > 1
						and
							(count(distinct POS) > 1 or count(distinct Norm) > 1 or count(distinct Number) > 1 or count(distinct Genre) > 1)
				)
			group by
				stat.Norm
			having
					count(distinct stat.{1})  > 1
				and
					count(distinct stat.POS) > 1
				and
					count(distinct stat.Word) > 1
	) t
group by
	{1}