select
    sum(Count) as Usages,
    sum(Sum) as Intersections
from
    (
        select
            Word,
            count(*) as Count,
            sum(Count) as Sum
        from
            {0}
        group by
            Word
        having
            (count(distinct POS) > 1 or count(distinct Norm) > 1 or count(distinct Number) > 1 or count(distinct Genre) > 1)
) t

union all

select
    sum(Count) as Usages,
    0
from
    {0}_tokens
where Word in
    (
        select
            Word
        from
            {0}
        group by
            Word
        having
            (count(distinct POS) > 1 or count(distinct Norm) > 1 or count(distinct Number) > 1 or count(distinct Genre) > 1)
	)
