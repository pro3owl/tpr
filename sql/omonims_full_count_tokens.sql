select
	sum(Count) as Usages
from
	{0}_tokens
where Word in
	(
        select
            Word
        from
            {0}
        group by
            Word
        having
            (count(distinct POS) > 1 or count(distinct Norm) > 1 or count(distinct Number) > 1 or count(distinct Genre) > 1)
) t

union all

select
	sum(Count) as Usages
from
	{0}_tokens
where Word in (
	select
		Word
	from
		{0}
	where Norm in (
        select
            Norm,
            count(*) as Count,
            sum(Count) as Sum
        from
            {0}
        where
            not Word in (
                select
                    Word
                from
                    {0}
                group by
                    Word
                having
                    (count(distinct POS) > 1 or count(distinct Norm) > 1 or count(distinct Number) > 1 or count(distinct Genre) > 1)
            )
        group by
            Norm
        having
                count(distinct POS) > 1
            and
                count(distinct Word) > 1
	) t1
) t