select
	sum(t.Count) as Count,
	sum(t.Sum) as Sum,
	sum(tokens.Count) as Tokens,
	group_concat(distinct t.Genres) as Genres,
	group_concat(distinct t.Numbers) as Numbers,
	t.{1}
from
		(
			select
				Word,
				group_concat({1}) as {1},
				count(*) as Count,
				sum(Count) as Sum,
				group_concat(distinct Genre) as Genres,
				group_concat(distinct Number) as Numbers
			from
				{0}
			group by
				Word
			having
					count(distinct {1}) > 1
				and not
					(
						count(distinct {2}) > 1
					or
						count(distinct {3}) > 1
					or
						count(distinct {4}) > 1
					)
		) t
	inner join
		{0}_tokens as tokens using(Word)
group by
	t.{1}