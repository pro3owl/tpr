import re
import pprint
import sys
import os
from utils import connect_local

pp = pprint.PrettyPrinter(indent=4)

IS_NUM = re.compile('\d')
NOT_WORDS_PARS = ['SENT', 'PT', 'PT_SENT', 'CARD', 'CM', 'FS', 'COLON']
NOT_WORDS_RE = re.compile('(' + '|'.join(NOT_WORDS_PARS) + ')')
IGNORE_WORDS = ['NUM']
IGNORE_WORDS_RE = re.compile('(' + '|'.join(IGNORE_WORDS) + ')')
IGNORE_NORMS = ['@card@']
IGNORE_NORMS_RE = re.compile('(' + '|'.join(IGNORE_NORMS) + ')')

NOT_WORDS_COUNT = 0
WORDS_COUNT = 0
UNKNOWN_WORDS_COUNT = 0
IGNORE_WORDS_COUNT = 0
SEVERAL_NORMS_COUNT = 0

POS_EXTRACTER = lambda pars: pos_from_spanish(pars)

POS_ITALIAN_RE = re.compile("^(?P<pos>[a-zA-Z]+)")
def pos_from_italian(pars):
	pos = POS_ITALIAN_RE.match(pars).group('pos')
	pos = pos.upper()
	if (pos == 'NPR'):
		return 'NOM'
	if (pos == 'NBC'):
		return 'ABR'
	return pos

POS_SPANISH_RE = re.compile("^(?P<pos>[A-Z]+)")
def pos_from_spanish(pars):
	if (pars[0] == 'V'):
		return 'V'
	if (pars[0] == 'C' and 'CARD' != pars):
		return 'C'
	if (pars == 'NEG'):
		return 'C'
	if (pars[0] == 'N'): #pars != 'NEG' and 
		return 'N'
	if (pars == 'INT' or pars == 'DM' or pars == 'REL' or pars[:2] == 'PP'):
		return 'PRONOUN'
	if (pars == 'ORD'):
		return 'CARD'
	if (pars == 'QU'):
		return 'ADJ'
	return POS_SPANISH_RE.match(pars).group('pos')
	
def init_lang(lang):
	global NOT_WORDS_PARS, NOT_WORDS_RE, IGNORE_WORDS, IGNORE_WORDS_RE, IGNORE_NORMS, IGNORE_NORMS_RE, POS_EXTRACTER
	if lang == 'it':
		NOT_WORDS_PARS = ['LS', 'PON', 'SENT', 'SYM', ]
		IGNORE_WORDS = ['FW', 'INT', 'NUM']
		IGNORE_NORMS = ['-']
		POS_EXTRACTER = lambda pars: pos_from_italian(pars)
	elif lang == 'es':
		NOT_WORDS_PARS = ['BACKSLASH', 'CM', 'CODE', 'COLON', 'DASH',
						  'DOTS', 'FO', 'FS', 'LP', 'PERCT',
						  'QT', 'RP', 'SEMICOLON', 'SLASH', 'SYM', '@card@']
		IGNORE_WORDS = ['ACRNM', 'ALFP', 'ALFS', 'ART', 'ITJN',
						'PAL', 'PDEL', 'PE', 'PNC', 'UMMX']
		IGNORE_NORMS = ['@card@']
		POS_EXTRACTER = lambda pars: pos_from_spanish(pars)
	else:
		raise Exception('Unknown or unsupported language: ' + lang)

	NOT_WORDS_RE = re.compile('(' + '|'.join(NOT_WORDS_PARS) + ')')
	IGNORE_WORDS_RE = re.compile('(' + '|'.join(IGNORE_WORDS) + ')')
	IGNORE_NORMS_RE = re.compile('(' + '|'.join(IGNORE_NORMS) + ')')


def is_word(word, pars, norm):
	global NOT_WORDS_COUNT, WORDS_COUNT, UNKNOWN_WORDS_COUNT, IGNORE_WORDS_COUNT
	unknown = '<unknown>' in norm
	if unknown:
		WORDS_COUNT += 1
		UNKNOWN_WORDS_COUNT += 1
	else:	
		if NOT_WORDS_RE.match(pars) or IS_NUM.match(word) :
			NOT_WORDS_COUNT += 1
		else:
			if IGNORE_WORDS_RE.match(pars) or IGNORE_NORMS_RE.match(norm) :
				WORDS_COUNT += 1
				IGNORE_WORDS_COUNT += 1
			else:
				WORDS_COUNT += 1
				return True
	
	return False

def get_summary():
	sum = WORDS_COUNT + NOT_WORDS_COUNT
	analyzing = WORDS_COUNT - IGNORE_WORDS_COUNT - UNKNOWN_WORDS_COUNT
	return [
		('all_tokens', sum),
		('words', WORDS_COUNT),
		('not_words', NOT_WORDS_COUNT),
		('all_words', WORDS_COUNT),
		('several_norms', SEVERAL_NORMS_COUNT),
		('unknown_words', UNKNOWN_WORDS_COUNT),
		('ignored_words', IGNORE_WORDS_COUNT),
		('perfect_words', analyzing),
	]

def make_statistics(filename, lang, load_to_db=True):
	global SEVERAL_NORMS_COUNT
	cur = connect_local()
	init_lang(lang)
	print('analyzing %s...' % (filename))
	f = open(filename)
	if load_to_db:
		to_mysql = open(filename + '.temp', 'w', encoding="utf-8")
	linenumber = 1
	for line in f :
		linenumber += 1
		if line[-1:] == '\n':
			line = line[:-1]
		array = line.split('\t')
		if len(array) != 3 :
			continue
		word = array[0]
		pars = array[1]
		norms = array[2].split("|")
		if len(norms) > 1: SEVERAL_NORMS_COUNT += 1

		for norm in norms:
			if is_word(word, pars, norm):
				if load_to_db:
					pos = POS_EXTRACTER(pars)
					to_mysql.write("%s\t%s\t%s\t%s\t%f\n" % (word.lower() , norm, pos.upper(), pars, 1.0 / len(norms)))

	if load_to_db:
		to_mysql_path = to_mysql.name
		to_mysql.close()
		load_data_to_db(lang, to_mysql_path, cur)
		print('Data was successfully loaded to database.')


def make_freeling(filename, lang, load_to_db=True):
	global SEVERAL_NORMS_COUNT
	cur = connect_local()
	init_lang(lang)
	print('analyzing %s...' % (filename))
	f = open(filename)
	if load_to_db:
		to_mysql = open(filename + '.temp', 'w', encoding="utf-8")
	linenumber = 1
	for line in f :
		linenumber += 1
		if line[-1:] == '\n':
			line = line[:-1]
		array = line.split('\t')
		if len(array) != 3 :
			continue
		word = array[0]
		pars = array[1]
		norms = array[2].split("|")
		if len(norms) > 1: SEVERAL_NORMS_COUNT += 1

		for norm in norms:
			if is_word(word, pars, norm):
				if load_to_db:
					pos = POS_EXTRACTER(pars)
					to_mysql.write("%s\t%s\t%s\t%s\t%f\n" % (word.lower() , norm, pos.upper(), pars, 1.0 / len(norms)))

	if load_to_db:
		to_mysql_path = to_mysql.name
		to_mysql.close()
		load_data_to_db(lang, to_mysql_path, cur)
		print('Data was successfully loaded to database.')
	
	


def load_data_to_db(lang, to_mysql_path, cur):
	tmplang = lang + "_tmp"
	create_lang_table(cur, lang)
	create_lang_table(cur, tmplang)

	os.system("""
		mysql -u root -D langs -e '
			load data local infile "{0}"
			into table {1}
			character set utf8
			fields terminated by "\t"
		'
	""".format(to_mysql_path, tmplang))

	cur.execute("""
		insert into `{0}` (
			Word,
			Norm,
			POS,
			Pars,
			Count
		)
		select
			Word,
			Norm,
			POS,
			Pars,
			sum(Count)
		from
			`{1}`
		group by
			Word,
			Norm,
			Pos,
			Pars
	""".format(lang, tmplang))

	cur.execute("""
		create table if not exists `stat` (
			`Lang` varchar(32) not null,
			`Stat` varchar(32) not null,
			`Value` double not null,
			primary key(`Lang`, `Stat`)
		) ENGINE=Aria DEFAULT CHARSET=utf8
	""")

	cur.execute("delete from `stat` where Lang = '{0}'".format(lang))

	cur.executemany("""
		insert into `stat` (
			`Lang`,
			`Stat`,
			`Value`
		) values ('{0}', %s, %s)
	""".format(lang), get_summary())

def create_lang_table(cur, name):
	cur.execute("drop table if exists `{0}`".format(name))
	cur.execute("""
		create table `{0}` (
			`Word` varchar(50) not null,
			`Norm` varchar(50) not null,
			`POS`  varchar(50) not null,
			`Pars` varchar(50) not null,
			`Count` float default 1,
			key (`Word`, `Norm`, `POS`, `Pars`),
			key (`Norm`, `Word`, `POS`, `Pars`)
		) ENGINE=Aria DEFAULT CHARSET=utf8
	""".format(name))
