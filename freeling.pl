#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use DBD::mysql;
use Getopt::Long;
use Unicode::Normalize;
use open ':encoding(utf8)';

binmode(STDOUT, ':encoding(utf8)');

our %GENRE = (
	A => 4,
	D => 4,
	N => 3,
	V => 7,
	P => 4,
	S => 4
);

our %NUMBER = (
	A => 5,
	D => 5,
	N => 4,
	V => 6,
	P => 5,
	S => 5
);

our $SKIP_POS = "WZF";
our $YES = 0;
our $LANG = '';
our $POSTFIX = "_new";
our $JUST_LOAD = 0;

my ($raw, $tagged);

GetOptions(
	"raw=s" => \$raw,
	"tagged=s" => \$tagged,
	"yes" => \$YES,
	"lang" => \$LANG,
	"postfix=s" => \$POSTFIX,
	"just_load" => \$JUST_LOAD
);

die "Need --raw or --tagged file to work" unless $raw || $tagged;

($LANG) = ($raw || $tagged) =~ /\b(es|it)\b/ unless $LANG;
die "Can't extract language from filename, set it explicitly with --lang" unless $LANG;



$tagged = freeling_raw($raw) if $raw;

if ($JUST_LOAD) {
	print "sorry, not implemented yet.."
} else {
	my ($tempfile, $tokens_tempfile, $all, $skipped, $multi, $good, $unknown, $norms) = proccess_tagged($tagged);
	load_to_db($tempfile, $tokens_tempfile, $all, $skipped, $multi, $good, $unknown, $norms);
}



exit 0;


sub freeling_raw {
	my $raw = shift;

	my $tagged = "$raw.fg";
	if ( !$YES && -e $tagged ) {
		say "There already exists '$tagged'. Want to rewrite?";
		1 while <STDIN> !~ /^(y|n)/i; # skip any line that doesn't start with y or n
		exit if $1 eq 'n';
	}
	system("cat $raw | analyze -f freeling-3.1/data/config/$LANG.cfg --fsplit freeling-3.1/data/$LANG/splitter.dat --outf morfo > $tagged");

	return $tagged;
}

sub proccess_tagged {
	my $tagged = shift;

	open(my $tfile, "<:encoding(utf8)", $tagged)
	or die "Can't open $tagged: $!";

	my $tempfile = "$tagged.temp";
	my $tokens_tempfile = "$tagged.tokens.temp";

	my $tokens = {};

	open(my $temp, ">:encoding(utf8)", $tempfile)
	or die "Can't open $tempfile: $!";

	open(my $temp_tokens, ">:encoding(utf8)", $tokens_tempfile)
	or die "Can't open $tokens_tempfile: $!";

	my ($all, $skipped, $multi, $good, $unknown, $norms) = (0, 0, 0, 0, 0, 0);
	while ( my $line = <$tfile> ) {
		next unless $line =~ /\S/;
		my ($word, @norms) = split /\s+/, $line;
		my $lnorms = 0;
		my $lskipped = 0;
		$word = lc($word);
		$word = NFKD($word);
		$word =~ s/\p{NonspacingMark}//g;

		$all++;

		$tokens->{$word}++;

		while ( @norms >= 3 && ( my ($norm, $params) = splice(@norms, 0, 3) ) ) {
			$norm = lc($norm);
			$norm = NFKD($norm);
			$norm =~ s/\p{NonspacingMark}//g;
			if ( $norm !~ /[\W_]/ && (my ($pos) = $params =~ /^([^$SKIP_POS])/) ) {
				say $temp join(
					"\t",
					$word,
					$norm,
					$pos,
					$params,
					$GENRE{$pos} ? substr($params, $GENRE{$pos} - 1, 1) || "-" : "-",
					$NUMBER{$pos} ? substr($params, $NUMBER{$pos} - 1, 1) || "-" : "-",
					1
				);
				$lnorms++;
			} else {
				$lskipped++;
			}
		}
		if ( $lnorms ) {
			$good++;
			$multi++ if $lnorms > 1;
			$norms += $lnorms;
		} elsif ( $lskipped ) {
			$skipped++;
		} else {
			$unknown++
		}
	}

	while ( my ($k, $v) = each %$tokens) {
        say $temp_tokens "$k\t$v";
    }

	close($temp);
	close($tfile);
	close($temp_tokens);

	return ($tempfile, $tokens_tempfile, $all, $skipped, $multi, $good, $unknown, $norms);
}

sub load_to_db {
	my $tempfile = shift;
	my $tokens_tempfile = shift;
	my $all = shift;
	my $skipped = shift;
	my $multi = shift;
	my $good = shift;
	my $unknown = shift;
	my $norms = shift;

	my $dbh = DBI->connect("DBI:mysql:database=langs;host=localhost", "root", "", {'RaiseError' => 1});

	$dbh->do("
		replace into
			stat (
				Lang,
				Stat,
				Value
			)
		values
			('$LANG$POSTFIX', 'all_tokens', $all),
			('$LANG$POSTFIX', 'known', " . ($all - $unknown) . "),
			('$LANG$POSTFIX', 'unknown', $unknown),
			('$LANG$POSTFIX', 'words', $good),
			('$LANG$POSTFIX', 'not_words', $skipped),
			('$LANG$POSTFIX', 'several_norms', $multi),
			('$LANG$POSTFIX', 'whole_norms', $norms)
	");

	my $temp_table = create_table($dbh, "${LANG}${POSTFIX}_temp", "stat");
	my $table = create_table($dbh, "$LANG$POSTFIX", "stat");
	my $actual_tokens_table = create_table($dbh, "${LANG}${POSTFIX}_tokens", "tokens");

	system(qq{mysql -u root -D langs -e '
		load data local infile "$tempfile"
		into table $temp_table
		character set utf8
		fields terminated by "\t"
	'});

	system(qq{mysql -u root -D langs -e '
		load data local infile "$tokens_tempfile"
		into table $actual_tokens_table
		character set utf8
		fields terminated by "\t"
	'});

	$dbh->do("
		insert into $table (
			Word,
			Norm,
			POS,
			Pars,
			Genre,
			Number,
			Count
		)
		select
			Word,
			Norm,
			POS,
			Pars,
			Genre,
			Number,
			sum(Count)
		from
			$temp_table
		group by
			Word,
			Norm,
			POS,
			Pars,
			Genre,
			Number
	");

	$dbh->do("
		replace into
			stat (
				Lang,
				Stat,
				Value
			)
		values
			('$LANG$POSTFIX', 'usage_with_genre', (select count(*) from $table where Genre != '-')),
			('$LANG$POSTFIX', 'usage_with_number', (select count(*) from $table where Number != '-')),
			('$LANG$POSTFIX', 'usage_with_genre_and_number', (select count(*) from $table where Genre != '-' and Number != '-')),
			('$LANG$POSTFIX', 'unique_usages', (select count(*) from $table))
	");
}

sub create_table {
	my $dbh = shift;
	my $table = shift;
	my $tabledef = shift;

	my %TABLEDEFS = (
		stat => '
			create table `$table` (
				`Word` varchar(50) not null,
				`Norm` varchar(50) not null,
				`POS` varchar(1) not null,
				`Pars` varchar(50) not null,
				`Genre` varchar(1) not null,
				`Number` varchar(1) not null,
				`Count` float default 1,
				KEY `Word` (`Word`,`Norm`,`POS`,`Pars`),
				KEY `Norm` (`Norm`,`Word`,`POS`,`Pars`)
			) ENGINE=Aria DEFAULT CHARSET=utf8
		',
		tokens => '
			create table `$table` (
				`Word` varchar(50) not null,
				`Count` float default 1,
				KEY `Word` (`Word`, `Count`)
			) ENGINE=Aria DEFAULT CHARSET=utf8
		'
	);

	$dbh->do("drop table if exists $table");
	(my $def = $TABLEDEFS{$tabledef}) =~ s/\$table/$table/g;
	$dbh->do($def);

	return $table;
}